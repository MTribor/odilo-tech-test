package es.mtribor.odilotechtest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OdiloTechTestApplication {

    public static void main(String[] args) {
        SpringApplication.run(OdiloTechTestApplication.class, args);
    }

}
