package es.mtribor.odilotechtest.service;

import es.mtribor.odilotechtest.controller.dto.CertificateDTO;
import es.mtribor.odilotechtest.repository.model.Certificate;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

public interface CertificateService {

    CertificateDTO uploadCertificateFile(final MultipartFile certificate, final String token,
                                         final long userId) throws IOException;

    CertificateDTO getCertificate(final long userId);

    Certificate getCertificateCompleteData(final long userId);

}
