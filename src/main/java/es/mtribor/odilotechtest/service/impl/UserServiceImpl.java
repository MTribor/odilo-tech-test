package es.mtribor.odilotechtest.service.impl;

import es.mtribor.odilotechtest.controller.dto.UserDTO;
import es.mtribor.odilotechtest.mapper.UserMapper;
import es.mtribor.odilotechtest.repository.UserRepository;
import es.mtribor.odilotechtest.repository.model.User;
import es.mtribor.odilotechtest.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    @Override
    @Transactional(readOnly = true)
    public UserDTO getUser(final long userId) {
        return Optional.of(userRepository.getById(userId))
                .map(UserMapper::convertUserToUserDTO)
                .orElseThrow(EntityNotFoundException::new);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<User> getUserDetailsByEmail(final String email) {
        return Optional.of(userRepository.getByEmail(email));
    }

    @Override
    @Transactional(readOnly = true)
    public UserDTO getUserByEmail(String email) {
        return Optional.of(userRepository.getByEmail(email))
                .map(UserMapper::convertUserToUserDTO)
                .orElseThrow(EntityNotFoundException::new);
    }

    @Override
    public boolean exists(long userId) {
        return userRepository.existsById(userId);
    }

}
