package es.mtribor.odilotechtest.service.impl;

import es.mtribor.odilotechtest.controller.dto.CustomUserDetails;
import es.mtribor.odilotechtest.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
public class CustomUserDetailsServiceImpl implements UserDetailsService {

    private final UserService userService;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        return userService.getUserDetailsByEmail(s).map(CustomUserDetails::new).get();
    }

}

