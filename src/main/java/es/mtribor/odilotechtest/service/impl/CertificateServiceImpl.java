package es.mtribor.odilotechtest.service.impl;

import es.mtribor.odilotechtest.controller.dto.CertificateDTO;
import es.mtribor.odilotechtest.mapper.CertificateMapper;
import es.mtribor.odilotechtest.repository.CertificateRepository;
import es.mtribor.odilotechtest.repository.model.Certificate;
import es.mtribor.odilotechtest.service.CertificateService;
import es.mtribor.odilotechtest.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.EntityNotFoundException;
import java.io.IOException;
import java.time.LocalDate;
import java.util.Optional;

@RequiredArgsConstructor
@Service
public class CertificateServiceImpl implements CertificateService {

    private final CertificateRepository certificateRepository;

    private final UserService userService;

    @Override
    public CertificateDTO uploadCertificateFile(final MultipartFile certificate, final String token, final long userId) throws IOException {
        byte[] data = certificate.getBytes();
        if (userService.exists(userId)) {
            Optional<Certificate> savedCert = Optional.ofNullable(certificateRepository.getByUserId(userId)).map(c -> {
                c.setToken(token);
                c.setData(data);
                c.setUploadDate(LocalDate.now());
                return c;
            });

            Certificate cert = savedCert.orElse(Certificate.builder()
                    .userId(userId)
                    .token(token)
                    .uploadDate(LocalDate.now())
                    .data(certificate.getBytes())
                    .build());

            savedCert = Optional.of(certificateRepository.save(cert));

            return savedCert
                    .map(CertificateMapper::convertCertificateToCertificateDTO)
                    .orElseThrow(InternalError::new);
        } else {
            throw new EntityNotFoundException();
        }
    }

    @Override
    public CertificateDTO getCertificate(final long userId) {
        return Optional.ofNullable(certificateRepository.getByUserId(userId))
                .map(CertificateMapper::convertCertificateToCertificateDTO)
                .orElseThrow(EntityNotFoundException::new);
    }

    @Override
    public Certificate getCertificateCompleteData(long userId) {
        return Optional.ofNullable(certificateRepository.getByUserId(userId))
                .orElseThrow(EntityNotFoundException::new);
    }

}
