package es.mtribor.odilotechtest.service.impl;

import es.mtribor.odilotechtest.controller.dto.UserDTO;
import es.mtribor.odilotechtest.repository.model.Certificate;
import es.mtribor.odilotechtest.service.CertificateService;
import es.mtribor.odilotechtest.service.FileSigningService;
import es.mtribor.odilotechtest.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

@Service
@RequiredArgsConstructor
public class FileSigningServiceImpl implements FileSigningService {

    private final CertificateService certificateService;
    private final UserService userService;

    @Override
    @Transactional(readOnly = true)
    public byte[] signFile(MultipartFile document, long userId) throws IOException {
        UserDTO userDTO = userService.getUser(userId);
        Certificate certificate = certificateService.getCertificateCompleteData(userId);

        String fileData = new String(document.getBytes(), StandardCharsets.UTF_8);
        String result = "#FIRMADO POR " +
                userDTO.getName() + " " + userDTO.getSurName() +
                " CON TOKEN DE VALIDACION " +
                certificate.getToken() +
                "#" +
                fileData;
        return result.getBytes();
    }

}
