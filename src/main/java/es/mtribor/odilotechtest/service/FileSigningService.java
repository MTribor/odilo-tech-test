package es.mtribor.odilotechtest.service;

import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

public interface FileSigningService {

    byte[] signFile(MultipartFile document, long userId) throws IOException;

}
