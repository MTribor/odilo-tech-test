package es.mtribor.odilotechtest.service;

import es.mtribor.odilotechtest.controller.dto.UserDTO;
import es.mtribor.odilotechtest.repository.model.User;

import java.util.Optional;

public interface UserService {

    UserDTO getUser(final long userId);

    Optional<User> getUserDetailsByEmail(final String email);

    UserDTO getUserByEmail(final String email);

    boolean exists(final long userId);

}
