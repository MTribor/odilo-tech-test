package es.mtribor.odilotechtest.repository;

import es.mtribor.odilotechtest.repository.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {

    User getByEmail(String email);

}
