package es.mtribor.odilotechtest.repository;

import es.mtribor.odilotechtest.repository.model.Certificate;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CertificateRepository extends JpaRepository<Certificate, Long> {

    Certificate getByUserId(long userId);

    String getTokenByUserId(long userId);

}
