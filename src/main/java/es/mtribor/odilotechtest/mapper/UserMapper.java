package es.mtribor.odilotechtest.mapper;

import es.mtribor.odilotechtest.controller.dto.UserDTO;
import es.mtribor.odilotechtest.repository.model.User;

public final class UserMapper {

    public static UserDTO convertUserToUserDTO(final User user) {
        return UserDTO.builder()
                .id(user.getId())
                .email(user.getEmail())
                .name(user.getName())
                .surName(user.getSurname())
                .build();
    }

}
