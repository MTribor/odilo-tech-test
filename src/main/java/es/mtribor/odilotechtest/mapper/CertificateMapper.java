package es.mtribor.odilotechtest.mapper;

import es.mtribor.odilotechtest.controller.dto.CertificateDTO;
import es.mtribor.odilotechtest.repository.model.Certificate;

public final class CertificateMapper {

    public static CertificateDTO convertCertificateToCertificateDTO(final Certificate certificate) {
        return CertificateDTO.builder()
                .id(certificate.getId())
                .userId(certificate.getUserId())
                .file(certificate.getData())
                .uploadDate(certificate.getUploadDate())
                .build();
    }

}
