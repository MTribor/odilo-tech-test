package es.mtribor.odilotechtest.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.persistence.EntityNotFoundException;
import java.io.IOException;

@RestControllerAdvice
public class ExceptionController {

    @ExceptionHandler(value = {EntityNotFoundException.class})
    protected ResponseEntity<Object> handleNotFoundException() {
        return ResponseEntity.notFound().build();
    }

    @ExceptionHandler(value = {IOException.class, InternalError.class})
    protected ResponseEntity<Object> handleIOException() {
        return ResponseEntity.unprocessableEntity().build();
    }

    @ExceptionHandler(value = {RuntimeException.class})
    protected ResponseEntity<Object> handleDefaultException() {
        return ResponseEntity.internalServerError().build();
    }

}
