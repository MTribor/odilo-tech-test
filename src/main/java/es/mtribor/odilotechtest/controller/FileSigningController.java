package es.mtribor.odilotechtest.controller;

import es.mtribor.odilotechtest.controller.apidoc.FileSigningApi;
import es.mtribor.odilotechtest.service.FileSigningService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.util.Objects;

@RequiredArgsConstructor
@CrossOrigin(maxAge = 3600)
@RestController
public class FileSigningController implements FileSigningApi {

    private final FileSigningService fileSigningService;

    @Override
    public ResponseEntity<byte[]> signFile(@NotNull MultipartFile document, long userId) throws IOException {
        return ResponseEntity.ok()
                .contentType(MediaType.valueOf(Objects.requireNonNull(document.getContentType())))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"SIGNED-" + document.getName() + "\"")
                .body(fileSigningService.signFile(document, userId));
    }
}
