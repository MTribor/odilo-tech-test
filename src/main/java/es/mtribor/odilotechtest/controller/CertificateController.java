package es.mtribor.odilotechtest.controller;

import es.mtribor.odilotechtest.controller.apidoc.CertificateApi;
import es.mtribor.odilotechtest.controller.dto.CertificateDTO;
import es.mtribor.odilotechtest.service.CertificateService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.net.URI;

@RequiredArgsConstructor
@CrossOrigin(maxAge = 3600)
@RestController
public class CertificateController implements CertificateApi {

    private final CertificateService certificateService;

    @Override
    public ResponseEntity<Object> uploadCertificateFile(@NotNull MultipartFile certificate, @NotEmpty String token, long userId) throws IOException {
        return ResponseEntity.created(URI.create(
                CertificateApi.ROOT_PATH
                        + "/"
                        + certificateService.uploadCertificateFile(certificate, token, userId).getId()))
                .build();
    }

    @Override
    public ResponseEntity<CertificateDTO> getCertificate(final long userId) {
        return ResponseEntity.ok(certificateService.getCertificate(userId));
    }
}
