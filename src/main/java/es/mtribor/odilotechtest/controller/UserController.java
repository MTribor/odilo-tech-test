package es.mtribor.odilotechtest.controller;

import es.mtribor.odilotechtest.controller.apidoc.UserApi;
import es.mtribor.odilotechtest.controller.dto.UserDTO;
import es.mtribor.odilotechtest.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor
@CrossOrigin(maxAge = 3600)
@RestController
public class UserController implements UserApi {

    private final UserService userService;

    @Override
    public ResponseEntity<UserDTO> getByEmail(final String email) {
        return ResponseEntity.ok(userService.getUserByEmail(email));
    }

}
