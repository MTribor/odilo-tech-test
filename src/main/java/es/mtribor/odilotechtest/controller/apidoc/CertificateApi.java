package es.mtribor.odilotechtest.controller.apidoc;

import es.mtribor.odilotechtest.controller.dto.CertificateDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RequestMapping(CertificateApi.ROOT_PATH)
@Api(value = "certificates", tags = "Certificate")
public interface CertificateApi {

    String ROOT_PATH = "/certificates";
    String USER_ID = "/{userId}";

    @ApiOperation(value = "Upload the certificate file")
    @ApiResponses({
            @ApiResponse(code = 201, message = "CREATED")
    })
    @PostMapping(USER_ID)
    ResponseEntity<Object> uploadCertificateFile(@RequestPart final MultipartFile certificate, @RequestParam final String token,
                                                 @PathVariable final long userId) throws IOException;

    @ApiOperation(value = "Gets the certificate metadata")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK")
    })
    @GetMapping(USER_ID)
    ResponseEntity<CertificateDTO> getCertificate(@PathVariable final long userId);

}
