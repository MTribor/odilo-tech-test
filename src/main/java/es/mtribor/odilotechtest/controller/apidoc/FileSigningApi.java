package es.mtribor.odilotechtest.controller.apidoc;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RequestMapping(UserApi.ROOT_PATH + FileSigningApi.USER_ID + FileSigningApi.ROOT_PATH)
@Api(value = "file-signing", tags = "File Signing")
public interface FileSigningApi {

    String ROOT_PATH = "/file-signing";
    String USER_ID = "/{userId}";


    @ApiOperation(value = "Upload and sign a document", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK")
    })
    @PostMapping
    ResponseEntity<byte[]> signFile(@RequestPart final MultipartFile document,
                                    @PathVariable final long userId) throws IOException;

}