package es.mtribor.odilotechtest.controller.apidoc;

import es.mtribor.odilotechtest.controller.dto.UserDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping(UserApi.ROOT_PATH)
@Api(value = "users", tags = "User")
public interface UserApi {

    String ROOT_PATH = "/users";
    String EMAIL = "/{email}";

    @ApiOperation(value = "Get the user basic data")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK")
    })
    @GetMapping(EMAIL)
    ResponseEntity<UserDTO> getByEmail(@PathVariable("email") final String email);

}
